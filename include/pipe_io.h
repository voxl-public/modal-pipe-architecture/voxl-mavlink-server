/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef PIPE_IO_H
#define PIPE_IO_H

#include <stdint.h>
#include <c_library_v2/common/mavlink.h>


int pipe_io_init(void);
int pipe_io_stop(void);


void pipe_io_en_print_debug_send(int en);
void pipe_io_en_print_debug_recv(int en);


// called when a msg is received from the onboard stream
// for onboard MPA services to read
int pipe_io_publish_onboard_msg(mavlink_message_t* msg);

// called when a msg is received from the AP intended for the GCS
// used to enable debugging what the AP is sending to GCS
int pipe_io_publish_to_gcs_msg(mavlink_message_t* msg);

// called when a msg is received from GCS
// intended for MPA services to sniff messages from GCS like custom commands
int pipe_io_publish_from_gcs_msg(mavlink_message_t* msg);

// called when a msg is received from a single stream
// that serves for both GCS and onboard. This is only used
// for APQ8096 where there is a single uart to an external PX4
// or on QRB5165 when using an external UART autopilot
int pipe_io_publish_to_both_channels(mavlink_message_t* msg);



#endif // end #define PIPE_IO_H
