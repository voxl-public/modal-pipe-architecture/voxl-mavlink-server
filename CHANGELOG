1.4.4
    * default audio zerorf mode to be off (silent)
1.4.3
    * support esc chimes and prop spin notifications for zerorf mode
1.4.2
    * allow different mavlink dialects between autopilot and gcs (don't discard crc-failures on that traffic)
1.4.1
    * bugfix in mission delay feature
1.4.0
    * add mission delay feature
    * make detecting of autopilot sysid more robust
    * fix bug where autopilot reporting unix time of 0 breaks linux system time
1.3.2
    * re-enable timsync on px4 slpi behind the en_timesync params
    * add note to config file header
1.3.1
    * remove libqrb5165-dependency for now to unlock APQ8096 builds
1.3.0
    * migrate to libqrb5165-io for uart
    * new dependency on libqrb5165-io >=0.4.0
1.2.1
    * fix bus error on APQ8096
1.2.0
    * new pipe mavlink_from_gcs
    * gcs timeout is configurable
    * new pipe mavlink_ap_heartbeat
1.1.0
    * allow heartbeats to be automatically sent to external autopilots
    * heartbeats configurable in config file
    * add 8.11 as default to second ip
1.0.5
    * gcs ip pipe now only shows active connections
1.0.4
    * add voxl-inspect-gcs-ip tool
1.0.3
    * don't start on configuration
1.0.2
    * fix thread safety between mavlink parsers
    * set MTU default to 0
    * rebuilt with new (april 2023) mavlink headers
1.0.1
    * fix gcs_ip_list not being published (probably)
1.0.0
    * restructure, bring in most of the logic from vvhub
0.3.0
    * on voxl1 allow auto-selecting from 2 uart ports
0.2.3
    * allow mavlink 1 messages through
0.2.2
    * fix bug with external_fc flag setting
0.2.1
    * fix external flight controller flag not being set
0.2.0
    * support external flight controller over UART on QRB5165
0.1.4
    * Added a simple UDP "reflector" to allow a QGC running on local host to connect to PX4
0.1.3
    * set control pipe threads ending data from MPA to PX4 as high RT priority
0.1.2
    * qrb5165 service no longer 'requires' voxl-px4, only after
0.1.1
    * New CI Stuff
0.1.0
    * first release
0.0.6
    * now build entirely in voxl-cross
0.0.5
    * different system service files for each hardware platform
0.0.4
    * add separate send/receive ports for mavlink UDP
0.0.3
    * cleanup
    * correct config file name
    * send test heartbeat on init for udp mode
0.0.2
    * check check mavlink magic before sending to pipe
    * write to pipe in one go from rpc shared mem
0.0.1
    * initial release
