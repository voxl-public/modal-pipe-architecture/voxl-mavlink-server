/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>	// for  close
#include <errno.h>

#include <modal_pipe.h>

#include "gcs_io.h"
#include "pipe_io.h"
#include "common.h"
#include "config_file.h"
#include "autopilot_interface.h"

//#define DEBUG_MTU

// assuming GCS sends one mavlink message per datagram, the UDP read buffer
// only needs to hold 1 little mavlink packet at a time. This is what we see.
// However, we make this buffer larger just in case a different GCS does many
// packets per datagram.
#define UDP_READ_BUF_LEN	(4096)
#define MAX_CONNECTIONS		16
#define LOCAL_ADDRESS_INT	2130706433
#define GCS_UDP_PORT		14550 // default in QGC

///////////////////////////////////////////////////////////////////////
// MISSION DELAY START FEATURE PX4 specific states
// These are defined in PX4 common message layer of the nav mgr
// for delayed mission management
///////////////////////////////////////////////////////////////////////
// base mode id to change to AUTO state as requested by any mavlink client. 
// Client requested AUTO state, see pX4 FlightMgr module, see pX4 flight_mode_manager module for details of this number
// See src/modules/commander/Commander.cpp: in PX4
// This is defined as [base_mode & VEHICLE_MODE_FLAG_AUTO_ENABLED] in PX4. 
#define MAV_STATE_AUTO 0x51
// custom mode id to change to MISSION mode as requested by any mavlink client. 
// Client requested MISSION mode, see pX4 flight_mode_manager module for details of this number
// See src/modules/commander/Commander.cpp: in PX4
// This is defined as [PX4_CUSTOM_SUB_MODE_AUTO_MISSION] in PX4. 
#define MAV_MODE_MISSION 0x4040000
//base mode id requested by any mavlink client. 
// Client requested ARMING state, see pX4 FlightMgr module, see pX4 flight_mode_manager module for details of this number
// See uORB/topics/vehicle_command.h  VEHICLE_CMD_COMPONENT_ARM_DISARM = 0x190
#define MAV_MODE_AUTO_ARM_DISARMED_POS 0x190


typedef struct mav_connection_t{
	int open; // set to 1 once this connection has an IP and sockaddr set, never set to 0 again
	int connected; // set to 1 once the first heartbeat is populated, set to 0 after heartbeat timeout
	struct sockaddr_in sockaddr;
	char ip_string[16];
	int64_t t_last_heartbeat_ns;
}mav_connection_t;

static int n_connections;
static mav_connection_t connections[MAX_CONNECTIONS];

// simple local variables
static int running;
static int print_debug_send;
static int print_debug_recv;

// if a mission delay timeout was given, enable the feature
static bool en_mission_delay = false;  		          	// if user defines a time > 0 this is enabled
static bool mission_delay_running = false;			// global state mgr prevent a request when it's already running' 	
static int mission_system_id;
static int mission_component_id;

// Socket stuff
static int sockfd_gcs;
static struct sockaddr_in si_me_gcs; // my sockaddr for talking to all gcs connections
static struct timeval tv; // for recv timeout
static pthread_t recv_thread_id_gcs;
static pthread_t timeout_thread_id;




// add a new IP to the list of connections, not necessarily a working connection,
// this is also called to add static IPs from config file
static int _add_connection(unsigned long ip)
{
	if(ip==LOCAL_ADDRESS_INT){
		fprintf(stderr,"ERROR, can't add localhost as a gcs connection\n");
		return -1;
	}

	// scan through current list of connections to see what to do
	for(int i=0;i<MAX_CONNECTIONS;i++){

		// already connected, do nothing
		if(connections[i].sockaddr.sin_addr.s_addr == ip) return 0;

		// got to the first closed connection, add it
		if(connections[i].open == 0){
			connections[i].sockaddr.sin_family = AF_INET; // IPv4
			connections[i].sockaddr.sin_port = htons(GCS_UDP_PORT);
			connections[i].sockaddr.sin_addr.s_addr = ip;
			sprintf(connections[i].ip_string,"%s",inet_ntoa(connections[i].sockaddr.sin_addr));
			connections[i].open = 1;
			n_connections++;
			printf("Added new UDP connection to %s\n", connections[i].ip_string);
			return 0;
		}
	}

	fprintf(stderr, "WARNING exceeded max UDP connections\n");
	return -1;
}


static void _make_and_publish_list(void)
{
	if(n_connections==0){
		pipe_server_write(GCS_IP_LIST_CH, "none", 5);
		return;
	}

	// compile list of IPs to publish to MPA
	// there should be at least one if we got here
	char buf[16*(n_connections+1)];
	buf[0]=0;
	for(int i=0;i<n_connections;i++){

		// skip known IPs that are not connected
		if(!connections[i].connected) continue;

		// make a space separated list of actually connected IPs
		strcat(buf, connections[i].ip_string);
		strcat(buf, " ");
	}


	// if there were no connections found, publish the text none
	if(strlen(buf)<1){
		pipe_server_write(GCS_IP_LIST_CH, "none", 5);
	}
	else{ // otherwise publish the list
		pipe_server_write(GCS_IP_LIST_CH, buf, strlen(buf)+1);
	}

	return;
}


// when receiving a heartbeat, add the ip as a connection, flag as connected
// since we actually got a message, and publish the updated list of IPs to MPA
static void _handle_heartbeat(unsigned long ip)
{
	if(_add_connection(ip)) return;

	// check if we recognize this ip
	for(int i=0;i<MAX_CONNECTIONS;i++){

		// stop searching once we've run out of open connections
		if(!connections[i].open) break;


		if(connections[i].sockaddr.sin_addr.s_addr == ip){
			if(!connections[i].connected){
				printf("CONNECTED to GCS at %s\n", connections[i].ip_string);
			}
			connections[i].connected = 1;
			connections[i].t_last_heartbeat_ns = my_time_monotonic_ns();
			break;
		}
	}

	_make_and_publish_list();
	return;
}


static void* _timeout_thread_func(__attribute__((unused)) void* arg)
{
	while(running){

		// get current time to compare with old heartbeat timestamps
		int64_t current_time_ns = my_time_monotonic_ns();

		// check all connections
		for(int i=0; i<MAX_CONNECTIONS; i++){

			// stop searching once we've run out of open connections
			if(!connections[i].open) break;

			// skip checking disconnected ips
			if(!connections[i].connected) continue;

			// check for timeout
			double age = (double)(current_time_ns-connections[i].t_last_heartbeat_ns) / 1000000000.0;
			if(age > gcs_timeout_s){
				printf("DISCONNECTED FROM GCS %s\n", connections[i].ip_string);
				connections[i].connected = 0;
				_make_and_publish_list();
			}
		}
		// check at 5hz
		usleep(200000);
	}
	return NULL;
}


void gcs_io_en_print_debug_send(int en_print_debug)
{
	printf("Enabling UDP GCS send debugging\n");
	print_debug_send = en_print_debug;
	return;
}

void gcs_io_en_print_debug_recv(int en_print_debug)
{
	printf("Enabling UDP GCS recv debugging\n");
	print_debug_recv = en_print_debug;
	return;
}


static int _send_to_all_external_udp_ports(uint8_t* buf, int bytes)
{
	int i;

	// send to any open connections
	for(i=0;i<MAX_CONNECTIONS;i++){
		if(connections[i].open){
			//printf("sending %d bytes to %s\n", bytes, connections[i].ip_string);
			sendto(sockfd_gcs, buf, bytes, MSG_CONFIRM, \
							(const struct sockaddr*) &connections[i].sockaddr, \
							sizeof(connections[i].sockaddr));
		}
	}

	return 0;
}



int gcs_io_send_to_gcs(mavlink_message_t* msg)
{
	static uint8_t* final_send_buf = NULL;
	static int n_in_final_send_buf = 0;
	static pthread_mutex_t udp_out_mtx = PTHREAD_MUTEX_INITIALIZER;

	// nothing to do if shutting down
	if(!running) return -1;

	// basic debugging help
	if(print_debug_send){
		printf("to GCS      msgid:%5d compid%4d\n", msg->msgid, msg->compid);
	}


	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int new_msg_len = mavlink_msg_to_send_buffer(buf, msg);

	// for the external udp ports, bundle up messages into bigger packets to
	// save on network traffic load

	// if mtu is less than max packet length, just send one UDP packet at a time
	// otherwise, add the mavlink message to the end of the queue to be sent
	if(udp_mtu<=MAVLINK_MAX_PACKET_LEN){
		#ifdef DEBUG_MTU
		fprintf(stderr, "sending %4d bytes (no mtu)\n", new_msg_len);
		#endif
		return _send_to_all_external_udp_ports(buf, new_msg_len);
	}

	// lock the output buffer mutex and stack the next message to send
	pthread_mutex_lock(&udp_out_mtx);

	// allocate memory for the buffer
	if(final_send_buf==NULL){
		final_send_buf = malloc(udp_mtu);
		if(final_send_buf==NULL){
			fprintf(stderr, "ERROR Failed to malloc udp send buffer\n");
			pthread_mutex_unlock(&udp_out_mtx);
			return -1;
		}
	}

	// check if the buffer would overflow, if so it's time to send
	if((n_in_final_send_buf+new_msg_len)>udp_mtu){
		#ifdef DEBUG_MTU
		fprintf(stderr, "sending %4d bytes\n", n_in_final_send_buf);
		#endif
		_send_to_all_external_udp_ports(final_send_buf, n_in_final_send_buf);
		n_in_final_send_buf = 0;
	}

	// add this message to the end of the buffer
	//fprintf(stderr,"in buf: %4d new_msg_len: %4d comb: %4d\n", n_in_final_send_buf, new_msg_len, n_in_final_send_buf+new_msg_len);
	memcpy(&final_send_buf[n_in_final_send_buf], buf, new_msg_len);
	n_in_final_send_buf+=new_msg_len;
	pthread_mutex_unlock(&udp_out_mtx);

	#ifdef DEBUG_MTU
	fprintf(stderr, "added %4d bytes to buffer msgid: %5d\n", new_msg_len, msg->msgid);
	#endif

	return 0;
}

static void audio_trigger(bool isError)
{
	if (autopilot_mission_delay_sound)
	{
		if (isError)
		{
			system("/usr/bin/voxl-send-esc-tone-cmd -f 1 -p 1 -d 100");
		}
		else
		{
			system("/usr/bin/voxl-send-esc-tone-cmd -f 500 -p 1 -d 40");
		}
	}

}

//
// this supports mavlink custom missions features, this runs on thread while mavlink server continues to process non-mission messages.
//
static void *_start_delayed_mission_thread_func(void *arg) {


	// Note we don't  need a mutex here as we want to drop/skip further requests
	// until this is done, while allowing buffer processing as this is kicked off in a buffer read cycle
	
	static mavlink_message_t msg;
	
	memset(&msg, 0, sizeof(mavlink_message_t));

	mavlink_message_t *in_msg = (mavlink_message_t *)arg;

	memcpy(&msg, in_msg, sizeof(mavlink_message_t));

	fprintf(stderr,"INFO: Mission Will Start in %d seconds\n", autopilot_mission_delay_start); 

	mavlink_message_t msg_motor;
	mavlink_command_long_t msg_cmd_motor;
        memset(&msg_cmd_motor, 0, sizeof(msg_cmd_motor));
        msg_cmd_motor.target_system =    1 ;
        msg_cmd_motor.target_component =  MAV_COMP_ID_AUTOPILOT1;
        msg_cmd_motor.command =          310 ;
        msg_cmd_motor.confirmation =     0;
        msg_cmd_motor.param1 =            0.01;
        msg_cmd_motor.param2 =            autopilot_mission_notif_dur;
        msg_cmd_motor.param3 =            0;
        msg_cmd_motor.param4 =            0;
        msg_cmd_motor.param6 =            0;
        msg_cmd_motor.param7 =            0;

	msg_cmd_motor.param5 =            1104.0;

	sleep(2);
	audio_trigger(false);

	/////////////////////////////////////
	/// Signal that command has been accepted and can now remove cable
	///////////////////sleep//////////////////
	sleep(2); //ack from slider
	mavlink_msg_command_long_encode(
										mission_system_id,
										mission_component_id,
										 &msg_motor,
										 &msg_cmd_motor);
	autopilot_io_send_gcs_msg(&msg_motor);
	pipe_io_publish_from_gcs_msg(&msg_motor);

	/////////////////////////////////////
	// wait for user delay
	/////////////////////////////////////
	// delay mission and arming here--we expect the user to disconnect and position
	// as needed.
	int init_delay = autopilot_mission_delay_start-10;
	if (init_delay < 0)
		init_delay = 10;
	sleep(init_delay);

	audio_trigger(false);

	/////////////////////////////////////
	// Notify that starting arming sequence!
	/////////////////////////////////////
	for (int i=0; i<10; i++)
	{
		if (!en_mission_delay)
		{
			audio_trigger(true);
			break;
		}

		sleep(1);

	    msg_cmd_motor.param5 =            1100.0 + ((i % 4)+1);

		mavlink_msg_command_long_encode(
	    									mission_system_id,
	                                        mission_component_id,
	                                         &msg_motor,
	                                         &msg_cmd_motor);
		autopilot_io_send_gcs_msg(&msg_motor);
		pipe_io_publish_from_gcs_msg(&msg_motor);

	}

	/////////////////////////////////////
	// LAUNCH
	/////////////////////////////////////
	// last check in case someone canceled programmatically
	if (en_mission_delay)
	{

		fprintf(stderr,"WARN: About to start mission!\n");
		
		// Put the 5 second delay here for motor spin to phsyically tell user that about to go
		
		fprintf(stderr,"WARN: Executing Delayed Mission Starting NOW!\n"); 
		
		// send to autopilot
		autopilot_io_send_gcs_msg(&msg);
		// send to pipe
		pipe_io_publish_from_gcs_msg(&msg);
	}
	else
	{
		audio_trigger(true);
		fprintf(stderr,"WARN: delay mission start WAS CANCELED by Operator!\n");
	}
	
	// global setting delay state to enable requests
	mission_delay_running = false;

	// reset trigger if something sets mission mode.
	en_mission_delay = false;	  
	
  	return NULL;
}

// thread to read from gcs_udp_port_number
static void* _recv_thread_func(__attribute__((unused)) void* arg)
{
	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	memset(&status, 0, sizeof(mavlink_status_t));
	memset(&msg, 0, sizeof(mavlink_message_t));
	int msg_received = 0;
	char buf[UDP_READ_BUF_LEN];
	struct sockaddr_in si_other;
	socklen_t slen = sizeof(si_other);

	// keep going until running becomes 0
	while(running){

		// Receive UDP message from ground control, this is blocking with timeout
		bytes_read = recvfrom(sockfd_gcs, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
								(struct sockaddr*)&si_other, &slen);

		//printf("bytes read: %d\n", bytes_read);

		// just woke up from a blocking read, check if we should exit
		if(!running) continue;

		// ignore EAGAIN error, that just means the timeout worked
		if(bytes_read<0 && errno!=EAGAIN){
			perror("ERROR: UDP recvfrom had a problem");
		}

		// ignore local packets
		if(si_other.sin_addr.s_addr == LOCAL_ADDRESS_INT){
			fprintf(stderr, "ERROR, received UDP packet from localhost on port %d\n", GCS_UDP_PORT);
			fprintf(stderr, "use port 14551 (routed through voxl-vision-hub) for local traffic instead\n");
		}

		// do the mavlink byte-by-byte parsing
		for(i=0; i<bytes_read; i++){
			// We don't care about CRC or Signature errors on packets
			// coming from the GCS because they may be a different Mavlink
			// dialect and are expected to fail CRC. Regardless, let's let
			// the autopilot decide for itself what to do with it.
			msg_received = mavlink_frame_char(MAV_CHAN_GCS, buf[i], &msg, &status);

			// check for dropped packets
			// TODO CHECK THIS, maybe bigger read buffer?
			if(status.packet_rx_drop_count != 0){
				fprintf(stderr,"WARNING: UDP socket from GCS dropped %d packets\n", status.packet_rx_drop_count);
				fprintf(stderr,"parse_err: %d packet_idx: %d flags: %d\n", status.parse_error, status.packet_idx, status.flags);
			}

			// msg_received indicates this byte was the end of a complete packet
			if(msg_received){
				if(print_debug_recv){
					printf("from GCS    ID:%4d port:%6d IP: %s \n",\
							msg.msgid, ntohs(si_other.sin_port), \
							inet_ntoa(si_other.sin_addr));
				}

				// for heartbeats, gcs might be trying to establish a new connection
				if(msg.msgid == MAVLINK_MSG_ID_HEARTBEAT){
					_handle_heartbeat(si_other.sin_addr.s_addr);
				}

				//
				// this block supports mavlink custom missions feature. Any mavlink client must
				// 1.  set a set mode request of auto and mission, this activates mission mode and enabled delayed start if delay
				//     is set
				// 2. waits for a arming command to start the countdown, arms and runs in mission mode when t=0
				//
				// This is a direct mirror of commands of what QGroundControl does when the user slides the "mission start" slider widget
				//
				if (autopilot_mission_delay_start > 0)
				{
					// A. QGC or mavlink clients 1st set to mission mode, this turns off RF timeout checks so the vehicle
					// runs normally and dosen't kick off failsafes
					if (msg.msgid == MAVLINK_MSG_ID_SET_MODE)
					{
						// check is mission mode was requested
						mavlink_set_mode_t new_mode;
						mavlink_msg_set_mode_decode(&msg, &new_mode);
						if (new_mode.base_mode == MAV_STATE_AUTO && 
							new_mode.custom_mode == MAV_MODE_MISSION)
						{
							en_mission_delay = true;
							fprintf(stderr,"INFO: delay mission start (%d secs) is activated!\n", 
								autopilot_mission_delay_start);
						}
						else
						{
							fprintf(stderr,"WARN: delay mission start CANCELED!\n");
							en_mission_delay = false; //CANCEL, we allow any mavlink state change to canceldelayed start
						}		
					}
					// B. then in a separate client call (subsequent) arming occurs. We delay the arming here.
					else if (msg.msgid == MAVLINK_MSG_ID_COMMAND_LONG)
					{

						mission_system_id = msg.sysid;
						mission_component_id = msg.compid;

						mavlink_command_long_t cmd_mavlink;
						mavlink_msg_command_long_decode(&msg, &cmd_mavlink);
						
						// if arm is called process delay logic, allows for canceling.
						if (cmd_mavlink.command == MAV_MODE_AUTO_ARM_DISARMED_POS)
						{
							// If mission mode was selected, run delay logic
							if (en_mission_delay)
							{
								pthread_t tid;
								if (!mission_delay_running)
								{
									mission_delay_running = true;
									if (pthread_create(&tid, NULL, _start_delayed_mission_thread_func, (void *)&msg) != 0) 
									{
										mission_delay_running = false;
										audio_trigger(true);
										fprintf(stderr,"INFO: delay mission start FAILED!\n");
									}
								}
								else
								{
									fprintf(stderr,"INFO: delay mission start ALREADY running (< %d seconds)!\n", autopilot_mission_delay_start);
								}
							}
							//never allow arming cause if you're left in mission mode  px4 will auto arm without delay!
							continue;  // IMPORTANT prevent buffer overflows.
						}
						else
						{
							// THIS ALLOWS PASS THRU of other mavlink messages than arming.
							en_mission_delay = false; // BUT CANCEL, we allow any mavlink state change to cancel the delayed start logic
						}
					}
				} //end mission delay logic

				
				// send to autopilot
				autopilot_io_send_gcs_msg(&msg);
				// send to pipe
				pipe_io_publish_from_gcs_msg(&msg);
			}
		}
	}

	printf("exiting gcs udp listener thread\n");
	return NULL;
}


// try to add new IPs received on the control pipe callback
static void _list_control_cb(__attribute__((unused)) int ch, char* string, \
							__attribute__((unused)) int bytes, \
							__attribute__((unused)) void* context)
{
	printf("gcs list pipe received command %s\n", string);

	struct in_addr new_ip;
	if(inet_aton(string, &new_ip)!=0){
		printf("Adding IP address to udp connection list: %s\n", string);
		_add_connection(new_ip.s_addr);
	}
	else printf("Not connecting to gcs ip: %s\n", string);

	return;
}


// publish the list as soon as a client connects
static void _list_connect_cb(__attribute__((unused)) int ch,\
							__attribute__((unused)) int client_id, \
							char* client_name, \
							__attribute__((unused)) void* context)
{
	_make_and_publish_list();
	printf("client \"%s\" connected to gcs list pipe\n", client_name);
	return;
}


int gcs_io_init(void)
{
	// IP list pipe
	pipe_info_t info = { \
		.name        = GCS_IP_LIST_NAME,\
		.location    = GCS_IP_LIST_NAME,\
		.type        = "string",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = 4096};

	pipe_server_set_control_cb(GCS_IP_LIST_CH, _list_control_cb, NULL);
	pipe_server_set_control_pipe_size(GCS_IP_LIST_CH, 1024, 1024);
	pipe_server_set_connect_cb(GCS_IP_LIST_CH, _list_connect_cb, NULL);
	pipe_server_create(GCS_IP_LIST_CH, info, SERVER_FLAG_EN_CONTROL_PIPE);


	// set up new socket
	sockfd_gcs = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd_gcs < 0) {
		perror("ERROR: UDP recv socket creation failed");
		return -1;
	}

	// set timeout for the socket
	tv.tv_sec = 0;
	tv.tv_usec = 500000;
	setsockopt(sockfd_gcs, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

	// set up my own sockaddr
	si_me_gcs.sin_family = AF_INET; // IPv4
	si_me_gcs.sin_port = htons(GCS_UDP_PORT);
	si_me_gcs.sin_addr.s_addr = htonl(INADDR_ANY);

	// bind socket to port
	if(bind(sockfd_gcs , (struct sockaddr*)&si_me_gcs, sizeof(si_me_gcs)) == -1){
		perror("ERROR: UDP socket failed to bind to port");
		return -1;
	}

	// add any of the 2 ips available for hard-coding in config file
	struct in_addr new_ip;
	if(inet_aton(primary_static_gcs_ip, &new_ip)!=0){
		printf("Adding primary GCS IP address from conf file to list: %s\n", primary_static_gcs_ip);
		_add_connection(new_ip.s_addr);
	}
	else printf("Not connecting to primary gcs ip: %s\n", primary_static_gcs_ip);

	// secondary manual ip
	if(inet_aton(secondary_static_gcs_ip, &new_ip)!=0){
		printf("Adding secondary manual gcs IP address to udp connection list: %s\n", secondary_static_gcs_ip);
		_add_connection(new_ip.s_addr);
	}
	else printf("Not connecting to secondary gcs ip: %s\n", secondary_static_gcs_ip);

	running = 1;
	pthread_create(&recv_thread_id_gcs, NULL, _recv_thread_func, NULL);
	pthread_create(&timeout_thread_id, NULL, _timeout_thread_func, NULL);

	return 0;
}


int gcs_io_stop(void)
{
	if(running==0){
		return 0;
	}

	running=0;
	pthread_join(recv_thread_id_gcs, NULL);
	close(sockfd_gcs);

	pthread_join(timeout_thread_id, NULL);

	pipe_server_close(GCS_IP_LIST_CH);

	printf("udp gcs io stopped\n");
	return 0;
}
